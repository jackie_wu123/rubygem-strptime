%global _empty_manifest_terminate_build 0
%global gem_name strptime
Name:		rubygem-strptime
Version:	0.2.5
Release:	1
Summary:	a fast strptime/strftime engine.
License:	BSD-2-Clause
URL:		https://github.com/nurse/strptime
Source0:	https://rubygems.org/gems/strptime-0.2.5.gem

BuildRequires:	ruby rsync
BuildRequires:	ruby-devel
BuildRequires:	rubygems
BuildRequires:	rubygems-devel
BuildRequires:	gcc
BuildRequires:	gdb
Provides:	rubygem-strptime

%description
a fast strptime/strftime engine which uses VM.

%package help
Summary:	Development documents and examples for strptime
Provides:	rubygem-strptime-doc
BuildArch: noarch

%description help
a fast strptime/strftime engine which uses VM.

%prep
%autosetup -n strptime-0.2.5
gem spec %{SOURCE0} -l --ruby > strptime.gemspec

%build
gem build strptime.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* %{buildroot}%{gem_dir}/
rsync -a --exclude=".*" .%{gem_dir}/* %{buildroot}%{gem_dir}/
if [ -d .%{_bindir} ]; then
	mkdir -p %{buildroot}%{_bindir}
	cp -a .%{_bindir}/* %{buildroot}%{_bindir}/
fi
if [ -d ext ]; then
	mkdir -p %{buildroot}%{gem_extdir_mri}/%{gem_name}
	if [ -d .%{gem_extdir_mri}/%{gem_name} ]; then
		cp -a .%{gem_extdir_mri}/%{gem_name}/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}
	else
		cp -a .%{gem_extdir_mri}/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}
fi
	cp -a .%{gem_extdir_mri}/gem.build_complete %{buildroot}%{gem_extdir_mri}/
	rm -rf %{buildroot}%{gem_instdir}/ext/
fi
rm -rf %{buildroot}%{gem_instdir}/{.clang-format,.gitignore,.rspec,.travis.yml}
pushd %{buildroot}
touch filelist.lst
if [ -d %{buildroot}%{_bindir} ]; then
	find .%{_bindir} -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%files -n rubygem-strptime -f filelist.lst
%dir %{gem_instdir}
%{gem_instdir}/*
%{gem_extdir_mri}
%exclude %{gem_cache}
%{gem_spec}

%files help
%{gem_docdir}/*

%changelog
* Mon Aug 02 2021 Ruby_Bot <Ruby_Bot@openeuler.org>
- Package Spec generated
